% passCountsOverview
%
% created 29Apr2017
% georgi.tushev@brain.mpg.de

clc
clear variables
close all

%% add path
path_project = pwd;
path_project = regexp(path_project, filesep, 'split');
path_project = sprintf('%s%s',...
                       filesep,...
                       fullfile(path_project{1:length(path_project)-2}));
addpath(genpath(path_project));


%{
%% load data
load passCounts_CultureTissueGlia_29Apr2017.mat;

%% split calculate minimum threshold
idx_filter = strcmp('filtered', data.feature) |...
             strcmp('mitochondria', data.feature) |...
             strcmp('intergenic', data.feature);
symbol_ref = data.symbol(~idx_filter);
feature_ref = data.feature(~idx_filter);

%% calculate reads per million
rpm = bsxfun(@rdivide, 1e6.*data.counts(~idx_filter,:), sum(data.counts(~idx_filter,:)));
idx_rpm_threshold = any(rpm >= 2, 2);

[featureLabel, ~, idx_feature] = unique(feature_ref(idx_rpm_threshold));
frq = histc(idx_feature, (1:max(idx_feature)));

[sym_unq, ~, idx_symbol] = unique(symbol_ref(idx_rpm_threshold));
frq_sym = histc(idx_symbol, (1:max(idx_symbol)));
%}
%{
%% minimum threshold is 2 reads per million
idx_glia = any(rpm(:,1:2)  >= 2, 2);
idx_nrnl = any(rpm(:,3:12) >= 2, 2);
idx_hipo = any(rpm(:,13:end) >= 2, 2);

sym_ref = data.symbol(~idx_filter);
sym_glia = unique(sym_ref(idx_glia));
sym_nrnl = unique(sym_ref(idx_nrnl));
sym_hipo = unique(sym_ref(idx_hipo));


%% intersect UTRs
idx_all = idx_glia | idx_nrnl | idx_hipo;
idx_ABC = idx_glia & idx_nrnl & idx_hipo;
idx_AB = idx_glia & idx_nrnl & ~idx_ABC;
idx_BC = idx_nrnl & idx_hipo & ~idx_ABC;
idx_AC = idx_glia & idx_hipo & ~idx_ABC;
idx_A = idx_glia & ~(idx_ABC | idx_AB | idx_BC | idx_AC);
idx_B = idx_nrnl & ~(idx_ABC | idx_AB | idx_BC | idx_AC);
idx_C = idx_hipo & ~(idx_ABC | idx_AB | idx_BC | idx_AC); 

%% intersect Genes
sym_all = unique([sym_glia;sym_nrnl;sym_hipo]);
sym_ABC = intersect(sym_glia,intersect(sym_nrnl,sym_hipo));
sym_AB = setdiff(intersect(sym_glia,sym_nrnl),sym_ABC);
sym_BC = setdiff(intersect(sym_nrnl,sym_hipo),sym_ABC);
sym_AC = setdiff(intersect(sym_glia,sym_hipo),sym_ABC);
sym_A = setdiff(sym_glia,unique([sym_ABC;sym_AB;sym_BC;sym_AC]));
sym_B = setdiff(sym_nrnl,unique([sym_ABC;sym_AB;sym_BC;sym_AC]));
sym_C = setdiff(sym_hipo,unique([sym_ABC;sym_AB;sym_BC;sym_AC]));

%% report
fprintf('A :: glia %d (%d)\n',sum(idx_glia),length(sym_glia));
fprintf('B :: neuronal culture %d (%d)\n',sum(idx_nrnl), length(sym_nrnl));
fprintf('C :: hippocampal tissue %d (%d)\n',sum(idx_hipo), length(sym_hipo));
fprintf('Union: %d / %d\n',sum(idx_all),length(sym_all));

fprintf('ABC: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_ABC), 100*sum(idx_ABC)/sum(idx_all),...
    length(sym_ABC), 100*length(sym_ABC)/length(sym_all));

fprintf('AB: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_AB), 100*sum(idx_AB)/sum(idx_all),...
    length(sym_AB), 100*length(sym_AB)/length(sym_all));

fprintf('BC: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_BC), 100*sum(idx_BC)/sum(idx_all),...
    length(sym_BC), 100*length(sym_BC)/length(sym_all));

fprintf('AC: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_AC), 100*sum(idx_AC)/sum(idx_all),...
    length(sym_AC), 100*length(sym_AC)/length(sym_all));

fprintf('A: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_A), 100*sum(idx_A)/sum(idx_all),...
    length(sym_A), 100*length(sym_A)/length(sym_all));

fprintf('B: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_B), 100*sum(idx_B)/sum(idx_all),...
    length(sym_B), 100*length(sym_B)/length(sym_all));

fprintf('C: %d (%.2f) / %d (%.2f)\n',...
    sum(idx_C), 100*sum(idx_C)/sum(idx_all),...
    length(sym_C), 100*length(sym_C)/length(sym_all));
%}

%% remove path
%rmpath(genpath(path_project));