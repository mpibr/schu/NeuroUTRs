% testCountsReader
%
% implements a test for passData.CountsReader class
%
% created 29Apr2017
% georgi.tushev@brain.mpg.de

clc
clear variables
close all

%% add path
path_project = pwd;
path_project = regexp(path_project, filesep, 'split');
path_project = sprintf('%s%s',...
                       filesep,...
                       fullfile(path_project{1:length(path_project)-1}));
addpath(genpath(path_project));

%% create an object
obj = passData.CountsReader('passCounts_CultureTissueGlia_29Apr2017.txt',...
                            'Samples','passCounts_SampleInfo_29Apr2017.txt');

%% read in counts                        
obj.read();

%{
idx_sample_smt = strncmp('TISSUE_SOMATA_CONTROL_',obj.samples,22);
idx_sample_npl = strncmp('TISSUE_NEUROPIL_CONTROL_',obj.samples,24);
idx_sample = idx_sample_smt | idx_sample_npl;
mtx = obj.counts(obj.index('RPM',2),idx_sample); 
lbl = obj.geneSymbol(obj.index('RPM',2));
scr = sum(mtx,2);

[lblunq, ~,lblidx] = unique(lbl);

rel = zeros(length(lblidx),1);
idx_use = false(max(lblidx),1);
check = 0;
for k = 1 : max(lblidx)
    idx_now = lblidx == k;
    prc_now = scr(idx_now)./sum(scr(idx_now));
    rel(idx_now) = prc_now;
    
    if sum(prc_now >= 0.2) >= 2
        check = check + 1;
        idx_use(k) = true;
    end
    
end

fw = fopen('/Users/tushevg/Desktop/listGenomeBrowser_07Jun2017.txt','w');
fprintf(fw,'%s\n',lblunq{idx_use});
fclose(fw);
%}
%{
figure('color','w');
hist(rel(rel < 1), 100);
%}
%% test index
%idx_camk2a = obj.index('GeneSymbol','Camk2a');

%idx_list = obj.index('GeneList',{'Camk2a','Calm1','Grin2a'});
%idx_tputr = obj.index('GeneFeature','3pUTR');
%idx_rpm = obj.index('RPM', 2);
%idx_rpmtosym = obj.index('RemapGeneSymbol',idx_rpm);
%}

%% export counts table
%{
idx_export_col = false(size(obj.counts,2), 1);
idx_export_col([13,14,21,22]) = true;
idx_export_row = idx_camk2a & idx_rpm;
obj.export('/Users/tushevg/Desktop/test.txt',...
           'Columns', idx_export_col,...
           'Rows',idx_export_row);
%}
%% export gene list


%% remove path
%rmpath(genpath(path_project));