#edgeR_createBatchFreeTable
#
# removes batch effect from
# sequencing counts table
#
# georgi.tushev@brain.mpg.de
# April 2017

# load edgeR
library(edgeR);

# clean current variables
rm(list = ls());

# read counts
indata<-as.matrix(read.table("/Users/tushevg/Desktop/NeuroUTRs/data/exports/exportPass_Tissue_12Jun2017.txt", stringsAsFactors=F,header=T,row.names=1));

# set factor levels
LabelTreatment<-c("CONTROL","CONTROL","BICU","BICU","CONTROLTI","CONTROLTI","BICUTI","BICUTI");
LevelTreatment<-factor(rep(LabelTreatment,2));
LevelTreatment<-relevel(LevelTreatment,ref="CONTROL");

LabelBatch<-c("1","2");
LevelBatch<-factor(rep(LabelBatch, 8));



# create DGE List
y<-DGEList(counts=indata, group=LevelTreatment);
y<-calcNormFactors(y);
plotMDS(y);

# batch effect
logCPM<-cpm(y, log=TRUE, prior.count=5);
plotMDS(logCPM);

logCPM<-removeBatchEffect(logCPM,batch=LevelBatch);
plotMDS(logCPM);

write.table(logCPM,"/Users/tushevg/Desktop/NeuroUTRs/data/edgeR/edgeR_TissueBatchFree_12Jun2017.txt",quote=FALSE,sep="\t",col.names=TRUE);

