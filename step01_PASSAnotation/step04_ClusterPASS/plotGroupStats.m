% plotReadStats
clc
clear variables
close all

%
% read reads raw counts
fRead = fopen('readsRawCounts.txt','r');
txt = textscan(fRead,'%s %n','delimiter','\t');
fclose(fRead);
keys_raw = txt{1};
reads_raw = txt{2};

% parse group stats
fRead = fopen('groupsStats_11Nov2016.txt','r');
txt = textscan(fRead, '%s %n %n %n %n %n %n','delimiter','\t');
keys_groups = txt{1};
counts_groups = [txt{2:4}];
reads_groups = [txt{5:7}];

% test keys
if ~all(strcmp(keys_raw,keys_groups(1:end-1)))
    fprintf('Error: keys mismatch!\n');
end

% plot stats
hdr = {'clustered','+seeds','-seeds'};
nrm_counts = bsxfun(@rdivide, reads_groups(1:end-1,:), reads_groups(1:end-1,1));
counts = counts_groups(1:end-1,:)./1e5;

% plot data
figure('Color','w',...
       'MenuBar','none',...
       'ToolBar','none',...
       'Position',[10,100,650,250]);

% plot base frequency
ha = axes('Position',[0.075,0.15,0.4150,0.83],'Units','normalized');

h = plotSpread(ha,counts, 'xNames', hdr);
hAxes = h{3};
hChildren = get(hAxes, 'Children');
colorCount = 100;
colorMap = colormap(jet(colorCount));
colorIndex = ceil(nrm_counts ./ (1/colorCount));
colorIndex = fliplr(colorIndex);
xValue = get(hChildren, 'XData');
yValue = get(hChildren, 'YData');
set(hChildren,'Color',[1,1,1]);
hold(hAxes,'on');
for k = 1 : size(counts,2)
    scatter(ha,xValue{k}, yValue{k}, 12, colorMap(colorIndex(:,k), :), 'filled');
end
hold(hAxes,'off');
set(hAxes,'Box','off',...
          'XLim',[0.5,size(counts,2)+0.5],...
          'FontSize',12);
hc = colorbar();
set(hc,'Ticks',(0:.1:1),...
       'TickLabels',(0:10:100));
ylabel(hc, 'reads relative fraction [%]','FontSize',12);
ylabel(hAxes, 'clusters [x 10^5]','FontSize',12);


% plot cluster span
fRead = fopen('spanPerCluster_11Nov2016.txt','r');
txt = textscan(fRead,'%n %n %n','delimiter','\t');
fclose(fRead);
span = txt{1};
freq = txt{2};
reads = txt{3};

hb = axes('Position',[0.58,0.15,0.4,0.83],'Units','normalized');
h = zeros(2,1);
hold on;
plot(hb,[log10(500),log10(500)],[0,1],'-.','Color',[.45,.45,.45]);
plot(hb,[log10(25),log10(25)],[0,1],'-.','Color',[.45,.45,.45]);
h(1) = plot(hb,log10(span),cumsum(freq./sum(freq)),'k','LineWidth',1.2);
h(2) = plot(hb,log10(span),cumsum(reads./reads_groups(end,1)),'Color',[148,0,211]./255,'LineWidth',1.2);
hold off;
text(log10(500)+0.05,0.1,'500 bases','Color',[.45,.45,.45],'HorizontalAlignment','left','VerticalAlignment','middle','FontSize',12);
text(log10(25)-0.05,0.1,'25 bases','Color',[.45,.45,.45],'HorizontalAlignment','right','VerticalAlignment','middle','FontSize',12);
set(hb,'Box','off');
xlabel(hb,'cluster length [log_1_0(bases)]','FontSize',12);
ylabel(hb,'cumulative fraction','FontSize',12);
hl = legend(h,'# of clusters','# of reads per cluster');
set(hl,'FontSize',12,'EdgeColor',[1,1,1],'Location','northoutside','Orientation','horizontal');

print(gcf,'-dpng','-r300','figureGroupsStats.png');
print(gcf,'-painters','-dsvg','-r300','figureGroupsStats.svg');
