#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub parseClusters($$);
sub printStats($);

MAIN:
{
    my $path_to_clusters = shift;
    my @files_with_clusters = glob("$path_to_clusters/*.bed.gz");
    my %stats = ();
    
    foreach my $file (@files_with_clusters)
    {
        parseClusters($file, \%stats);
    }

    printStats(\%stats);
}

sub printStats($)
{
    my $stats_ref = $_[0];
    
    foreach my $file (sort keys %{$stats_ref})
    {
        my $name = basename($file);
        $name =~ s/\_R1\.MACE\.cleaned\_window25\_cluster.bed.gz//g;
        print $name;
        print "\t",$stats_ref->{$file}{"counts"}{"groups"};
        print "\t",$stats_ref->{$file}{"counts"}{"+seeds"};
        print "\t",$stats_ref->{$file}{"counts"}{"-seeds"};
        print "\t",$stats_ref->{$file}{"reads"}{"groups"};
        print "\t",$stats_ref->{$file}{"reads"}{"+seeds"};
        print "\t",$stats_ref->{$file}{"reads"}{"-seeds"};
        print "\n";
        
    }
}


sub parseClusters($$)
{
    my $file = $_[0];
    my $stats_ref = $_[1];
    
    open(my $fh,"bgzip -@ 6 -d -c $file | ") or die $!;
    while(<$fh>)
    {
        chomp($_);
        my @line = split("\t", $_, 15);
        
        $stats_ref->{$file}{"counts"}{"groups"}++;
        $stats_ref->{$file}{"reads"}{"groups"} = exists($stats_ref->{$file}{"reads"}{"groups"}) ? ($stats_ref->{$file}{"reads"}{"groups"} + $line[8]) : $line[8];
        
        # check groups with/without seeds
        if ($line[11] == 0)
        {
            $stats_ref->{$file}{"counts"}{"-seeds"}++;
            $stats_ref->{$file}{"reads"}{"-seeds"} = exists($stats_ref->{$file}{"reads"}{"-seeds"}) ? ($stats_ref->{$file}{"reads"}{"-seeds"} + $line[8]) : $line[8];
        }
        else
        {
            $stats_ref->{$file}{"counts"}{"+seeds"}++;
            $stats_ref->{$file}{"reads"}{"+seeds"} = exists($stats_ref->{$file}{"reads"}{"+seeds"}) ? ($stats_ref->{$file}{"reads"}{"+seeds"} + $line[8]) : $line[8];
        }
        
    }
    close($fh);
}




