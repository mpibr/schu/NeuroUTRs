#!/usr/bin/bash

# $1 :: input directory with gbed.gz files
# $2 :: window size
# $3 :: output directory

for file in $1*.gbed.gz
do
file_name=$(basename "$file");
file_name="${file_name%.*.*}";
file_out=$3$file_name"_window"$2"_cluster.bed.gz";
>&2 echo $file_name;
# run PASSCluster
T="$(date +%s)";
perl PASSCluster.pl -gbed $file -window $2 | sort -k1,1 -k2,2n | bgzip > $file_out;
tabix --sequence 1 --begin 2 --end 2 --zero-based $file_out;
T="$(($(date +%s)-T))";
>&2 echo "Time in seconds: ${T}";

done
