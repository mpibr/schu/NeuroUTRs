#!/usr/bin/perl

use warnings;
use strict;

sub runningAverage($$$);

my $cluster_file = shift;

my $test_cluster_count = 0;
my $test_chrom_start = 0;
my $test_avg_span = 0;
my $test_avg_base = 0;
my $test_acm_base = 0;
my $test_acm_tail = 0;
my $test_acm_poly = 0;
my $test_base_best = 0;
my $test_base_pass = 0;


open(my $fh, "<", $cluster_file) or die $!;
while (<$fh>)
{
    chomp($_);
    
    my @line = split("\t", $_);
    
    
    $test_cluster_count++;
    $test_chrom_start++ if($line[1] < 0);
    runningAverage(\$test_avg_span, $line[4], $test_cluster_count);
    runningAverage(\$test_avg_base, $line[7], $test_cluster_count);
    $test_acm_base += $line[8];
    $test_acm_tail += $line[12];
    $test_acm_poly += $line[6];
    $test_base_best++ if($line[7] > 0 && $line[10] == -1);
    $test_base_pass++ if($line[11] > 0 && $line[14]== -1);
    
}
close($fh);

print "Test01: cluster count ",$test_cluster_count,"\n";
print "Test02: negativechrom start ",$test_chrom_start,"\n";
print "Test03: bases ",$test_acm_base,"\n";
print "Test04: tails ",$test_acm_tail,"\n";
print "Test05: polies ",$test_acm_poly,"\n";
print "Test06: avg.span ",$test_avg_span,"\n";
print "Test07: avg.base ",$test_avg_base,"\n";
print "Test08: base.best ",$test_base_best,"\n";
print "Test09: base.pass ",$test_base_pass,"\n";


sub runningAverage($$$)
{
    my $avg_ref = $_[0];
    my $val = $_[1];
    my $idx = $_[2];
    
    ${$avg_ref} = (($idx - 1)/$idx) * ${$avg_ref} + (1 / $idx) * $val;
    
    return;
}

