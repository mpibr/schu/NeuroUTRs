#!/usr/bin/perl

use warnings;
use strict;

sub parseReferenceFile($$);
sub parsePASSFile($$);

MAIN:
{
    my $ref_file = shift;
    my $pass_file = shift;
    my %ref_hash = ();
    
    parseReferenceFile(\%ref_hash, $ref_file);
    parsePASSFile(\%ref_hash, $pass_file);
}

sub parsePASSFile($$)
{
    my $ref_hash = $_[0];
    my $pass_file = $_[1];
    
    open(my $fh, "gunzip -c $pass_file |") or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        my @line = split("\t", $_, 20);
        if(exists($ref_hash->{$line[3]}))
        {
            my $chrom = $line[0];
            my $chrom_start = $line[-3];
            my $chrom_end = $line[14];
            
            # swap coordinates
            if ($chrom_start > $chrom_end)
            {
                my $swap = $chrom_end;
                $chrom_end = $chrom_start;
                $chrom_start = $swap;
            }
            
            
            my $name = $line[3] . ";" . $ref_hash->{$line[3]}[0] . ";" . $ref_hash->{$line[3]}[1] . ";" . $ref_hash->{$line[3]}[2];
            my $score = $line[-2];
            my $strand = $line[5];
            
            print $chrom,"\t",$chrom_start,"\t",$chrom_end,"\t",$name,"\t",$score,"\t",$strand,"\n";
        }
 
    }
    close($fh);
    
}

sub parseReferenceFile($$)
{
    my $ref_hash = $_[0];
    my $ref_file = $_[1];
    
    open(my $fh, "<", $ref_file) or die $!;
    while(<$fh>)
    {
        chomp($_);
        
        my ($passid, $symbol, $isoform, $celltype) = split("\t", $_, 4);
        $ref_hash->{$passid} = [$symbol, $isoform, $celltype];
    }
    close($fh);
}
