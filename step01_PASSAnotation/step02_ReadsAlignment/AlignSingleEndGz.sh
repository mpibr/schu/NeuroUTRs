#!/bin/bash

# $1 :: source directory
# $2 :: STAR genome dir
# $3 :: BAM dir out
# $4 :: LOG dir out


for file in $1*.fastq.gz
do
	file_name=$(basename "$file");
	file_name="${file_name%.*.*}";
	echo $file_name;
	echo "alignment ...";
STAR --runMode alignReads --runThreadN 6 --genomeDir $2 --readFilesIn $file --readFilesCommand zcat --outSAMattributes All --outStd Log --outSAMtype BAM SortedByCoordinate --outSAMstrandField intronMotif --outFilterIntronMotifs RemoveNoncanonical --alignSoftClipAtReferenceEnds No --outFilterScoreMinOverLread 0.25 --outFilterMatchNminOverLread 0.25;

    echo "index alignment ...";
        mv "Aligned.sortedByCoord.out.bam" $3$file_name".bam";
        samtools index $3$file_name".bam";

	echo "cleanup ...";
    	mv "Log.final.out" $4$file_name"_seqlog.txt";
    	rm -f "SJ.out.tab";
    	rm -f "Log.out";
    	rm -f "Log.progress.out";
done
