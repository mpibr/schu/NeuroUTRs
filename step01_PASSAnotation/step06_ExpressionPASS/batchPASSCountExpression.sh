#!/bin/bash

# $1 :: source directory
# $2 :: reference PASS annotation
# $3 :: counted expression dir out

for file in $1*.gbed.gz
do
	file_name=$(basename "$file");
	file_name="${file_name%.*.*}";
    echo $file_name;
    gunzip -c $file | awk -F"\t" 'OFS="\t"{print $1, ($2 - 1), $2, NR, $4, $3}' | sort -k1,1 -k2,2n | bedtools closest -a - -b $2 -s -D b -t first | perl PASSCountExpression.pl > $3$file_name"_passcounts.txt";
done
