#!/usr/bin/perl

use warnings;
use strict;
use Bio::DB::HTS::Tabix;
use File::Basename;
use List::MoreUtils qw(pairwise);

sub TabixCreate($$);
sub TabixDestroy($);
sub TabixParse($$$);
sub RegionsParse($$$);
sub ReportHeader($);
sub ReportTable($);


MAIN:
{
    my $regions_file = shift;
    my $tabix_path = shift;
    my @tabix_obj = ();
    my %table = ();
    
    TabixCreate(\@tabix_obj, $tabix_path);
    
    ReportHeader(\@tabix_obj);
    
    RegionsParse(\%table, \@tabix_obj, $regions_file);
    
    TabixDestroy(\@tabix_obj);
    
    ReportTable(\%table);

}

# subroutines
sub ReportHeader($)
{
    my $tabix_obj = $_[0];
    
    print "#passid\tsymbol\tfeature\twindow\tspan\treads";
    foreach my $record (@{$tabix_obj})
    {
        print "\t",$record->[1];
    }
    print "\n";
    
    return;
}


sub ReportTable($)
{
    my $table = $_[0];
    
    foreach my $key (keys %{$table})
    {
        my $total = 0;
        
        for (@{$table->{$key}})
        {
            $total += $_;
        }
        
        print "<skip>","\t","<skip>","\t",$key,"\t",-1,"\t",-1,"\t",$total,"\t",join("\t",@{$table->{$key}}),"\n";
    }
}


sub RegionsParse($$$)
{
    my $table = $_[0];
    my $tabix_obj = $_[1];
    my $regions_file = $_[2];
    
    my $test = 0;
    my $offset = 0;
    my $error_flag = 0;
    
    # initialize table
    my @alloc_ary = (0) x scalar(@{$tabix_obj});
    $table->{"intergenic"} = \@alloc_ary;
    $table->{"filtered"} = \@alloc_ary;
    $table->{"mitochondria"} = \@alloc_ary;
    
    open(my $fh,"gunzip -c $regions_file |") or die $!;
    while (<$fh>)
    {
        # split line
        chomp($_);
        my @line = split("\t", $_, 20);
        
        # define regions
        my $region = $line[0] . ":" . ($line[1]+1) . "-" . $line[2];
        my $strand = $line[5];
        
        # query regions
        my ($counts, $total) = TabixParse($tabix_obj, $region, $strand);

        # assign to table
        if (($line[-1] == 0) && ($line[16] eq "intergenic"))
        {
            $table->{"intergenic"} = [pairwise {$a + $b} @{$table->{"intergenic"}}, @{$counts}];
        }
        elsif (($line[-1] == 0) && ($line[16] ne "intergenic"))
        {
            $table->{"filtered"} = [pairwise {$a + $b} @{$table->{"filtered"}}, @{$counts}];
        }
        elsif (($line[-1] == 1) && ($line[0] eq "chrM"))
        {
            $table->{"mitochondria"} = [pairwise {$a + $b} @{$table->{"mitochondria"}}, @{$counts}];
        }
        elsif (($line[-1] == 1) && ($line[16] ne "chrM"))
        {
            print $line[3],"\t",$line[15],"\t",$line[16],"\t",$line[4],"\t",$line[18],"\t",$total,"\t",join("\t",@{$counts}),"\n";
        }
 
        if ($total != $line[8])
        {
            $test++;
            $offset+= abs($total - $line[8]);
            $error_flag = 1;
        }
        
        if($error_flag == 1)
        {
            print STDERR "ERROR: ",$test,"\t","OFFSET: ",$offset,"\n";
            last;
        }
        
    }
    close($fh);
    
}

sub TabixCreate($$)
{
    my $tabix_obj = $_[0];
    my $tabix_path = $_[1];
    
    my @tabix_files = glob("$tabix_path/*.gbed.gz");
    foreach my $file (@tabix_files)
    {
        my $name = fileparse($file,"_R1.MACE.cleaned.gbed.gz");
        my $obj = Bio::DB::HTS::Tabix->new( filename => $file);
        push(@{$tabix_obj},[$obj, $name]);
    }
    
    return;
}


sub TabixDestroy($)
{
    my $tabix_obj = $_[0];
    
    foreach my $obj (@{$tabix_obj})
    {
        $obj->[0]->close;
    }
    
}

sub TabixParse($$$)
{
    my $tabix_obj = $_[0];
    my $region = $_[1];
    my $strand = $_[2];
    my @counts = ();
    my $total = 0;
    
    # loop through each object
    foreach my $obj (@{$tabix_obj})
    {
        my $iter = $obj->[0]->query($region);
        my $value = 0;
        while (my $base = $iter->next)
        {
            my @line = split("\t", $base, 6);
            $value += $line[3] if($line[2] eq $strand);
        }
        push(@counts, $value);
        $total+= $value;
    }
    
    return (\@counts, $total);
}


