% plotReadsQuality
clc
clear variables
close all


queryDir = '/Volumes/Project Groups/RNA/Data/RNASequencing/GeneXPro/umiclean/seqstats/';

% read input files
infoDir = dir([queryDir '*_seqstats.txt']);
listFiles = {infoDir.name}';
listSize = size(listFiles, 1);

% parse each file
read_length = 1000;
data_mtx = zeros(read_length, 6, listSize);
for f = 1 : listSize
    
    fileIn = [queryDir filesep listFiles{f}];
    
    % parse file
    fRead = fopen(fileIn, 'r');
    txt = textscan(fRead, '%s', 'delimiter', '\n');
    fclose(fRead);
    txt = txt{:};
    idx = strncmp('#base', txt, 5);
    idx_keep = cumsum(idx);
    idx_keep(idx) = 0;
    txt(~idx_keep) = [];
    
    txt = textscan(sprintf('%s\n',txt{:}), '%n %n %n %n %n %n %n %n %n %n','delimiter','\t');
    base = txt{1};
    acgt = [txt{2:5}];
    phred = txt{9} - 33;
    sder = txt{10};
    
    data_mtx(1:size(acgt,1),1:4,f) = acgt;
    data_mtx(1:size(phred,1),5,f) = phred;
    data_mtx(1:size(sder,1),6,f) = sder;
end

% remove empty bases
idx = any(sum(data_mtx, 3), 2);
data_mtx(~idx,:,:) = [];

% plot data
figure('Color','w',...
       'MenuBar','none',...
       'ToolBar','none',...
       'Position',[10,100,650,250]);

% plot base frequency
ha = axes('Position',[0.075,0.15,0.4150,0.83],'Units','normalized');
clr_mtx = [255,69,0;...
           30,144,255;...
           50,205,50;...
           199,21,133]./255;
hold(ha,'on');
plot(ha,[0,size(data_mtx,1)],[0.25,0.25],'-.','Color',[.75,.75,.75]);
hh = zeros(4,1);
for k = 1 : 4
    
    med = mean(data_mtx(:,k,:), 3);
    ste = std(data_mtx(:,k,:),[],3);
    lwq = med - ste;
    upq = med + ste;
    
    idx = (1 : size(data_mtx,1) - 1)';
    
    X = [idx; flipud(idx)];
    
    h = fill(ha,X,[lwq(idx);flipud(upq(idx))],clr_mtx(k,:));
    set(h,'EdgeColor','none','FaceAlpha',0.25);
    hh(k,:) = plot(ha,idx, med(idx), 'Color', clr_mtx(k,:),'LineWidth',1.2);
    
end
hold(ha,'off');
hl = legend(hh,'A','C','G','T');
set(hl,'EdgeColor','w','FontSize',12,'Location','northwest');

set(ha,'Box','off',...
        'YLim',[0,0.5],...
        'YTick',[0.1,0.2,0.25,0.3,0.4,0.5],...
        'XTick',[1,10:10:floor(size(data_mtx,1)/10)*10,size(data_mtx,1)]);
xlabel(ha,'read length [nts]','FontSize',12);
ylabel(ha,'base frequency [relative]','FontSize',12);

% plot read score
hb = axes('Position',[0.565,0.15,0.4150,0.83],'Units','normalized');
hold(hb,'on');
med = mean(data_mtx(:,5,:), 3);
ste = mean(data_mtx(:,6,:),3);
lwq = med - ste;
upq = med + ste;
%med = prctile(data_mtx(:,5,:),50,3);
%lwq = prctile(data_mtx(:,5,:),1,3);
%upq = prctile(data_mtx(:,5,:),99,3);
idx = (1 : size(data_mtx,1) - 1)';
X = [idx; flipud(idx)];
h = fill(hb,X,[lwq(idx);flipud(upq(idx))],[0.5,0.5,0.5]);
set(h,'EdgeColor','none','FaceAlpha',0.25);
plot(hb,idx, med(idx), 'Color', 'k','LineWidth',1.2);
    
hold(hb,'off');
set(hb,'Box','off',...
        'YLim',[0,45],...
        'YTick',(0:10:45),...
        'XLim',[0,size(data_mtx,1)],...
        'XTick',[1,10:10:floor(size(data_mtx,1)/10)*10,size(data_mtx,1)]);
xlabel(hb,'read length [nts]','FontSize',12);
ylabel(hb,'Phred score [-10*log_1_0(P)]','FontSize',12);

print(gcf,'-opengl','-dpng','-r300','figureReadsQuality.png');
print(gcf,'-painters','-dsvg','-r300','figureReadsQuality.svg');



