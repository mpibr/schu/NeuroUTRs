% plotClustersToFeatures
clc
clear variables
close all

fRead = fopen('clustersToFeatures_22Nov2016.txt','r');
txt = textscan(fRead,'%s %n %n','delimiter','\t');
fclose(fRead);
feature = txt{1};
feature = regexprep(feature,'\_','\-');
count = txt{2};
accm = txt{3};
idx_srt = [1,2,3,4,5,7,6];


pcount = count ./ sum(count);
paccm = accm ./ sum(accm);
mtx = [pcount,paccm];


colorData = [128,128,128;148,0,211]./255;

figure('Color','w',...
       'MenuBar','none',...
       'ToolBar','none',...
       'Position',[10,100,325,250]);
h = bar(mtx(idx_srt,:));
set(h(1),'FaceColor',colorData(1,:),'EdgeColor','none');
set(h(2),'FaceColor',colorData(2,:),'EdgeColor','none');

set(gca,'Box','off',...
        'XTick',(1:size(feature,1)),...
        'XTickLabel',feature(idx_srt),...
        'XTickLabelRotation',-45);
ylabel('relative fraction [count / total]','FontSize',12);
hl = legend(h,'# clusters','# reads');
set(hl,'FontSize',12,'EdgeColor',[1,1,1],'Location','NorthWest');
print(gcf, '-dpng','-r300','figureClustersToFeatures.png');
print(gcf,'-painters','-dsvg','-r300','figureClustersToFeatures.svg');
