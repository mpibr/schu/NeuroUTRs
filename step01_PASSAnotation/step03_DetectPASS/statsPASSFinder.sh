#!/usr/bin/bash

# $1 :: path to PASSFinder output

for file in "$1"*.gbed.gz
do
file_name=$(basename "$file");
file_name="${file_name%.*.*}";
bgzip -d -c -@ 16 "$file" | awk -F"\t" -v label=$file_name 'OFS="\t"{algn+=$4;tail+=$5;if($6==1)masked+=$4;if($1=="chrM")mito+=$4}END{print label,algn,tail,masked,mito}'

done


