#!/usr/bin/bash

# $1 :: path to PASSFinder
# $2 :: input directory with bam files
# $3 :: poly regions file
# $4 :: output directory file

for file in $2*.bam
do
file_name=$(basename "$file");
file_name="${file_name%.*}";
file_out=$4$file_name".gbed.gz";
>&2 echo $file_name;
# run PASSFinder
T="$(date +%s)";
$1PASSFinder --input $file -r $3 --masksize 3 --polysize 3 --mapq 255 | sort -k1,1 -k2,2n | bgzip -@ 8 > $file_out;
tabix --sequence 1 --begin 2 --end 2 --zero-based $file_out;
T="$(($(date +%s)-T))";
>&2 echo "Time in seconds: ${T}";


done


