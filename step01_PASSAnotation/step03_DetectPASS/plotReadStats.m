% plotReadStats
clc
clear variables
close all

% parse stats
fRead = fopen('readStats_09Nov2016.txt','r');
hdr = fgetl(fRead);
hdr = regexp(hdr,'\t','split');
hdr = hdr(2:end)';
txt = textscan(fRead,'%s %n %n %n %n %n','delimiter','\t');
fclose(fRead);
file_name = txt{1};
counts = [txt{2:end}] ./ 1e6;

% normalize counts
nrm_counts = bsxfun(@rdivide, counts, counts(:,1));

% plot data
figure('Color','w',...
       'MenuBar','none',...
       'ToolBar','none',...
       'Position',[10,100,650,250]);
h = plotSpread(counts, 'xNames', hdr);
hAxes = h{3};
hChildren = get(hAxes, 'Children');
colorCount = 100;
colorMap = colormap(jet(colorCount));
colorIndex = ceil(nrm_counts ./ (1/colorCount));
colorIndex = fliplr(colorIndex);
xValue = get(hChildren, 'XData');
yValue = get(hChildren, 'YData');
set(hChildren,'Color',[1,1,1]);
hold(hAxes,'on');
for k = 1 : size(counts,2)
    scatter(xValue{k}, yValue{k}, 12, colorMap(colorIndex(:,k), :), 'filled');
end
hold(hAxes,'off');
set(hAxes,'Box','off',...
          'XLim',[0.5,size(counts,2)+0.5],...
          'FontSize',12);
hc = colorbar();
set(hc,'Ticks',(0:.1:1),...
       'TickLabels',(0:10:100));
ylabel(hc, 'relative fraction [%]','FontSize',12);
ylabel(hAxes, 'reads [x million]','FontSize',12);
print(gcf,'-dpng','-r300','figureReadsStats.png');
print(gcf,'-painters','-dsvg','-r300','figureReadsStats.svg');



