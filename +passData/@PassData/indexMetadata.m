function idx = indexMetadata(obj, varargin)
%INDEXIDS index PASS ids based on criteria
%   INPUT
%       GeneSymbol:         query gene name is indexed
%       GeneList:           query gene list is indexed
%       GeneFeature:        query gene feature is indexed
%       RemapGeneSymbol:    takes index and remapts it based on symbol
%
%   georgi.tushev@brain.mpg.de
%   April 2017
    
    %% only one indexing criteria is allowed
    if nargin > 3
        error('PassData.indexMetadata :: only one indexing criteria is allowed');
    end

    
    %% check if metadata is initialized
    if isempty(obj.passid)
        error('PassData.indexMetadata :: metadata not initialized');
    end
    
    
    %% parse varargin
    pobj = inputParser;
    addParameter(pobj, 'GeneSymbol', [], @ischar);
    addParameter(pobj, 'GeneList', [], @iscellstr);
    addParameter(pobj, 'GeneFeature', [], @ischar);
    addParameter(pobj, 'RemapGeneSymbol', [], @islogical);
    parse(pobj, varargin{:});
    
    
    %% assign values
    geneSymbol = pobj.Results.GeneSymbol;
    geneList = pobj.Results.GeneList;
    geneFeature = pobj.Results.GeneFeature;
    remapIndex = pobj.Results.RemapGeneSymbol;
    
    
    %% default return is all
    idx = true(size(obj.passid, 1), 1);
    
    
    %% GeneSymbol
    if ~isempty(geneSymbol) && ~isempty(obj.geneSymbol)
        
        idx = strcmp(obj.geneSymbol, geneSymbol);
        
    end
    
    
    %% GeneList
    if ~isempty(geneList) && ~isempty(obj.geneSymbol)
        
        [symRef, ~, idxUnique] = unique(obj.geneSymbol);
        symQry = unique(geneList);
        [~, idxIsect] = intersect(symRef, symQry);
        idxRef = false(size(symRef,1), 1);
        idxRef(idxIsect) = true;
        idx = idxRef(idxUnique);
        
    end
    
    
    %% GeneFeature
    if ~isempty(geneFeature) && ~isempty(obj.Feature)
        
        idx = strcmp(obj.geneFeature, geneFeature);
        
    end
 
    
    %% Remap index to gene symbols
    if ~isempty(remapIndex) && ~isempty(obj.geneSymbol)
        
        [symRef, ~, idxUnique] = unique(obj.geneSymbol);
        symQry = unique(obj.geneSymbol(remapIndex));
        [~,idxIsect] = intersect(symRef, symQry);
        idxRef = false(size(symRef,1),1);
        idxRef(idxIsect) = true;
        idx = idxRef(idxUnique);
        
    end

end

