classdef PassData < handle
    %PASSDATA read/write pass data
    %   metadata describing poly(A) supported sites (PASS)
    %
    % April 2017
    % georgi.tushev@brain.mpg.de
    
    %% --- metadata properties -- %%
    properties (SetAccess = protected)
        
        fileFullName;      % full filename, together with absolute path
        passid;            % pass unique identifier
        geneSymbol;        % gene symbol associated with the PASS
        geneFeature;       % gene feature asspcoated with the PASS
        coverageWindow;    % coverage window of the PASS
        upstreamLength;    % span from closest upstream gene feature (3'UTR length)
        
    end
    
    properties (SetAccess = protected, Hidden = true)
        
        filePath;          % path containing the file
        fileName;          % filename without path
        fileExt;           % file extension
        
    end
    
    methods
        function obj = PassData(filename)
        %PASSDATA PassData Constructor
        % Performs basic assignments common to all classes
        % INPUT
        %   filename: the file to open, a char array
        % OUTPUT
        %   obj: the returned PassData object
        % SEE ALSO
        %   passData.readCounts, passData.readEdgeR
            
            pobj = inputParser;
            pobj.addRequired('filename', @ischar);
            pobj.parse(filename);
            obj.fileFullName = which(filename);
            [obj.filePath, obj.fileName, obj.fileExt] = fileparts(obj.fileFullName);
            
        end
        
        function delete(~)
        %DELETE PassData Destructor
        end
        
    end
    
    methods (Abstract = true)
    % here we have the methods that each subclass MUST implement
        
        data = read(obj);
        idx = index(obj, varargin);
        export(obj, filename, varargin);
        
    end
    
    methods (Access = protected)
    %% implemented in seperate file
    
        idx = indexMetadata(obj, varargin);
        
    end
    
end

