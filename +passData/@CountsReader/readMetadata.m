function obj = readMetadata(obj)
%READMETADATA Read metadata for pass data
%   this method parses the metadata from passCount file
%
%   georgi.tushev@brain.mpg.de
%   April 2017

    %% open file
    filePtr = fopen(obj.fileFullName, 'r');
    if filePtr == -1
        error('CountsReader.readMetadata :: could not open file to read');
    end
    
    %% read header line
    headerLine = fgetl(filePtr);
    headerLine = regexp(headerLine, '\t', 'split');

    %% create metadata format
    metaFormat = repmat({'%*s'}, length(headerLine), 1);
    metaFormat(1:obj.fieldsMeta) = {'%s'};
    metaFormat = sprintf('%s ', metaFormat{:});
    metaFormat(end) = [];

    %% read meta data
    metaText = textscan(filePtr, metaFormat, 'delimiter', '\t');
    
    %% close file
    fclose(filePtr);
    
    %% parse meta data column
    for m = 1 : obj.fieldsMeta
        switch m
            case 1
                obj.passid = metaText{m};
            case 2
                obj.geneSymbol = metaText{m};
            case 3
                obj.geneFeature = metaText{m};
            case 4
                obj.coverageWindow = metaText{m};
            case 5
                obj.upstreamLength = metaText{m};
        end
        
    end

end

