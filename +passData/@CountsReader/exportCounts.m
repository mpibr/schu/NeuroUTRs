function exportCounts(obj, filename, varargin)
%EXPORTCOUNTS writes to file subset of count data
%   INPUT
%       filename: name of file to write
%       Header: [{true}, false] define if header is required
%       Columns: index of required columns, default all
%       Rows: index of required rows, default all
%
%   georgi.tushev@brain.mpg.de
%   April 2017
    
    %% parse varargin
    pobj = inputParser;
    addRequired(pobj, 'filename', @ischar);
    addParameter(pobj, 'Header', true, @islogical);
    addParameter(pobj, 'Rows', true(size(obj.counts, 1), 1), @islogical);
    addParameter(pobj, 'Columns', true(size(obj.counts, 2), 1), @islogical);
    parse(pobj, filename, varargin{:});
    
    useHeader = pobj.Results.Header;
    indexRows = pobj.Results.Rows;
    indexColumns = pobj.Results.Columns;
    
    %% open file for writing
    filePtr = fopen(filename, 'w');
    if filePtr == -1
        error('CountsReader.exportCounts :: could not open file to write');
    end
    
    %% write out header
    if useHeader
        fprintf(filePtr, '#passid');
        fprintf(filePtr, '\t%s',obj.samples{indexColumns});
        fprintf(filePtr, '\n');
    end
    
    %% get a subset matrix
    exportLabels = obj.passid(indexRows);
    exportCounts = obj.counts(indexRows, indexColumns);
    exportData = [exportLabels, num2cell(exportCounts)]';
    
    %% construct export format
    exportFormat = repmat({'%d\t'},sum(indexColumns) + 1, 1);
    exportFormat(1) = {'%s\t'};
    exportFormat = sprintf('%s',exportFormat{:});
    exportFormat(end) = 'n';
    
    %% write out data
    fprintf(filePtr, exportFormat, exportData{:});
    
    %% close file after writing
    fclose(filePtr);

end

