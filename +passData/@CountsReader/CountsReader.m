classdef CountsReader < passData.PassData
    %COUNTSREADER Class used to read pass counts
    %   this class implements a MatLab API for reading files
    %   resulting from pass counter procedure.
    %   The constructor of the class will do the heavy part of the job
    %   namely parsing the metadata required from the PassData library
    %
    %   georgi.tushev@brain.mpg.de
    %   April 2017
    %   SEE ALSO passData.PassData
    
    properties
        
        counts;         % matrix of all counts
        samples;        % sample labels
        
    end
    
    properties (Hidden = true)
        
        fileSamples;    % file with sample info
        fieldsMeta;     % number of columns for metadata 
        fieldsCounts;   % number of columns for counts
        fieldsSkip;     % index of columns to skip
        
    end
    
    methods
        function obj = CountsReader(filename, varargin)
        %COUNTSREADER Class constructor
        %
        %   the constructor calls the constructor of the superclass,
        %   and then tries to parse the file to extract as much
        %   information as possible from the file.
        %
        %SEE ALSO passData.PassData
        
            
            %% parse columns
            pobj = inputParser;
            addParameter(pobj, 'Metadata', 5, @isnumeric);
            addParameter(pobj, 'Counts', 28, @isnumeric);
            addParameter(pobj, 'Skip', 1, @isnumeric);
            addParameter(pobj, 'Samples', [], @ischar);
            parse(pobj, varargin{:});
            
            %% call explicitly because we pass one argument
            obj = obj@passData.PassData(filename);
            
            %% assign abstract methods
            obj.fieldsMeta = pobj.Results.Metadata;
            obj.fieldsCounts = pobj.Results.Counts;
            obj.fieldsSkip = pobj.Results.Skip;
            obj.fileSamples = pobj.Results.Samples;
        
            %% set as many properties from the superclass as possible
            obj = obj.readMetadata();
            
        end
        
        function delete(~)
        %DELETE Class destructor
        end
        
        function obj = read(obj)
        %READ extract counts data
        %   this method re-implements abstract method 
        %   readCounts and readSample are evoked
        
            obj.readCounts();
            
            if ~isempty(obj.fileSamples)
                obj.readSamples();
            end
            
        end
        
        function idx = index(obj, varargin)
        %INDEX index PASS ids based on given criteria
        %   this method re-implements abstract method
        %   indexIDs is evoked
            
            idx = obj.indexCounts(varargin{:});
        
        end
        
        function export(obj, filename, varargin)
        %EXPORT exports defined data from object
        %   this  method re-implements abstract method
        %   exportCounts is evoked
        
            obj.exportCounts(filename, varargin{:});
        
        end
    end
    
    methods (Access = protected)
    %% implemented in seperate files
    
        obj = readMetadata(obj);
        obj = readCounts(obj);
        obj = readSamples(obj);
        
        idx = indexCounts(obj, varargin);
        
        exportCounts(obj, filename, varargin);
        
    end
    
end

