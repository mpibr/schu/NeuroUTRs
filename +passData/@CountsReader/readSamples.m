function obj = readSamples(obj)
%READSAMPLES Read sample info
%   this method parses the info for samples defined in passCount file
%
%   georgi.tushev@brain.mpg.de
%   April 2017

    %% open file
    filePtr = fopen(obj.fileSamples, 'r');
    if filePtr == -1
        error('CountsReader.readSamples :: could not open file to read');
    end
    
    %% read sample info
    textInfo = textscan(filePtr, '%s %s %n', 'delimiter', '\t');
    labelOld = textInfo{1};
    labelNew = textInfo{2};
    rankQry = textInfo{3};
    
    %% intersect with current sample labels
    [~, idx_ref, idx_qry] = intersect(obj.samples, labelOld);
    obj.samples(idx_ref) = labelNew(idx_qry);
    rankRef = (1:length(obj.samples))';
    rankRef(idx_ref) = rankQry(idx_qry);

    %% resort based on query index
    [~,idx_sort] = sort(rankRef);
    obj.samples = obj.samples(idx_sort);
    obj.counts = obj.counts(:,idx_sort);
    
end

