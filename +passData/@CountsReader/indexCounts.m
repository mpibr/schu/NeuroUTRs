function idx = indexCounts(obj, varargin)
%INDEXIDS index counts
%   INPUT
%       RPM:                threshold for reads per million
%       Columns:            index of columns
%
%   georgi.tushev@brain.mpg.de
%   April 2017
    
    %% parse varargin
    pobj = inputParser;
    addParameter(pobj, 'RPM', 2, @isnumeric);
    addParameter(pobj, 'Columns', true(size(obj.counts, 2), 1), @islogical);
    parse(pobj, varargin{:});
    
    
    %% assign values
    rpm = pobj.Results.RPM;
    columnIndex = pobj.Results.Columns;
    
    
    %% calculate reads per million
    idx_filter = strcmp('<skip>', obj.passid);
    rpmCounts = bsxfun(@rdivide,...
                       1e6 .* obj.counts(:, columnIndex),...
                       sum(obj.counts(~idx_filter, columnIndex)));
    
         
    %% apply threshold
    idx = any(rpmCounts >= rpm, 2);
    idx(idx_filter) = false;

end

