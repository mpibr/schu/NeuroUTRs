function obj = readCounts(obj)
%READCOUNTSTABLE Read counts from pass table
%   this method parses the counts from passCount file
%
%   georgi.tushev@brain.mpg.de
%   April 2017


    %% create header format
    headerFormat = repmat({'%s'}, obj.fieldsMeta + obj.fieldsSkip + obj.fieldsCounts, 1);
    headerFormat(1:(obj.fieldsMeta + obj.fieldsSkip)) = {'%*s'};
    headerFormat = sprintf('%s ', headerFormat{:});
    headerFormat(end) = [];
    
    %% create table format
    tableFormat = repmat({'%n'}, obj.fieldsMeta + obj.fieldsSkip + obj.fieldsCounts, 1);
    tableFormat(1:(obj.fieldsMeta + obj.fieldsSkip)) = {'%*s'};
    tableFormat = sprintf('%s ', tableFormat{:});
    tableFormat(end) = [];

    %% open file
    filePtr = fopen(obj.fileFullName, 'r');
    if filePtr == -1
        error('CountsReader.readCounts :: could not open file to read');
    end
    
    %% read header
    textHeader = textscan(filePtr, headerFormat, 1, 'delimiter', '\t');
    
    %% read table
    textTable = textscan(filePtr, tableFormat, 'delimiter', '\t');
    
    %% close file
    fclose(filePtr);
    
    %% assign header
    obj.samples = [textHeader{:}]';
    
    %% assign counts
    obj.counts = [textTable{1:end}];
    
end

